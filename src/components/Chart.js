import React from 'react';
import {Box, Flex, Text,} from "@chakra-ui/react";
import PropTypes from "prop-types";
import {BarChart, Bar, CartesianGrid, Tooltip, XAxis, YAxis} from "recharts";

const YearPreview = ({firstYear, firstData, secondYear, secondData}) => {

    const snowCm = (data, monthNum) => {
        return Object.values(data[monthNum]).slice(0, -4).reduce((a, b) => a + b, 0);
    }
    console.log(firstYear, secondYear)

    const y1 = [{
            "name": "Listopad",
            "cm": snowCm(firstData, 0)
        },
        {
            "name": "Prosinec",
            "cm": snowCm(firstData, 1)
        },
        {
            "name": "Leden",
            "cm": snowCm(firstData, 2)
        },
        {
            "name": "Únor",
            "cm": snowCm(firstData, 3)
        },
        {
            "name": "Březen",
            "cm": snowCm(firstData, 4)
        }];
    const y2 = [{
        "name": "Listopad",
        "cm": snowCm(secondData,0)
        },
        {
            "name": "Prosinec",
            "cm": snowCm(secondData,1)
        },
        {
            "name": "Leden",
            "cm": snowCm(secondData,2)
        },
        {
            "name": "Únor",
            "cm": snowCm(secondData,3)
        },
        {
            "name": "Březen",
            "cm": snowCm(secondData,4)
        }];

    return (
        <Flex justifyContent={'space-evenly'}>
            <Box>
                <Text textAlign='center'>{firstYear}</Text>
                <BarChart width={480} height={350} data={y1}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis domain={[0, 500]} label={{ value: 'sníh (cm)', angle: -90, position: 'insideLeft' }} />
                    <Tooltip />
                    <Bar dataKey="cm" fill="#8884d8" />
                </BarChart>
            </Box>
            <Box>
                <Text textAlign='center'>{secondYear}</Text>
                <BarChart width={480} height={350} data={y2} mx={'1em'}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis domain={[0, 500]} />
                    <Tooltip />
                    <Bar dataKey="cm" fill="#8884d8" />
                </BarChart>
            </Box>
        </Flex>
    );
};

export default YearPreview;

YearPreview.propTypes = {
    firstYear: PropTypes.number,
    firstData: PropTypes.arrayOf(
        PropTypes.object
    ),
    secondYear: PropTypes.number,
    secondData: PropTypes.arrayOf(
        PropTypes.object
    ),
};
