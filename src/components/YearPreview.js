import React from 'react';
import {Box, Card, CardBody, Table, TableContainer, Tbody, Td, Tr} from "@chakra-ui/react";
import PropTypes from "prop-types";

const YearPreview = ({year, data}) => {
    const snowCm = (monthNum) => {
        return Object.values(data[monthNum]).slice(0, -4).reduce((a, b) => a + b, 0);
    }

    const tdStyle = {
        borderLeft: 'white 1px solid',
        borderRight: 'white 1px solid'
    }
    return (
        <Card alignItems='center' justifyContent='center' w='full' background='rgba(89, 126, 160, 0.15)'
            border='1px solid #FFFFFF' borderRadius='10px'>

            <CardBody>
            <Box alignSelf={'flex-start'} position='absolute' left='0' top='0'
                 backgroundColor='#E21217'
                 width='83px' height='40px'
                 borderRadius='0px 0px 16px 0px'
                color='white'
                textAlign='center'
                 padding='5px'
                    fontSize='15pt'
                 marginTop='-1px'
                marginLeft='-1px'
                >
                {year}
            </Box>
            <Box textAlign={'center'} color={'#2D39A8'} fontSize='1.6em' m={'2em'}>
                <TableContainer>
                    <Table size='lg' variant='unstyled'>
                        <Tbody>
                            <Tr borderTop='white 1px solid'>
                                <Td {...tdStyle}>listopad</Td>
                                <Td {...tdStyle} w={'5em'}>{snowCm(0)}</Td>
                            </Tr>
                            <Tr>
                                <Td {...tdStyle}>prosinec</Td>
                                <Td {...tdStyle}>{snowCm(1)}</Td>
                            </Tr>
                            <Tr>
                                <Td {...tdStyle}>leden</Td>
                                <Td {...tdStyle}>{snowCm(2)}</Td>
                            </Tr>
                            <Tr>
                                <Td {...tdStyle}>unor</Td>
                                <Td {...tdStyle}>{snowCm(3)}</Td>
                            </Tr>
                            <Tr borderBottom='white 1px solid'>
                                <Td {...tdStyle}>brezen</Td>
                                <Td {...tdStyle}>{snowCm(4)}</Td>
                            </Tr>
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
            </CardBody>
        </Card>
    );
};

export default YearPreview;

YearPreview.propTypes = {
    year: PropTypes.number,
    data: PropTypes.arrayOf(
        PropTypes.object
    ),

};
