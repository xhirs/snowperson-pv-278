import React from 'react';
import { CloseIcon } from '@chakra-ui/icons'
import {
    Avatar,
    Button,
    Flex, HStack,
    Link,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalOverlay, Text, useDisclosure, VStack
} from "@chakra-ui/react";
import {Link as ReactLink, useLocation} from 'react-router-dom';
import {useAuth0} from "../react-auth0-spa";

export const headerHeight = '6em';

const TopBar = () => {
    const { isAuthenticated, user, loginWithRedirect, logout} = useAuth0();
    const location = useLocation();
    console.log(user);
    const { isOpen, onOpen, onClose } = useDisclosure();

    return (
        <>
            <Flex as='header' height={headerHeight} alignItems='center' justifyContent='right' zIndex='2'>
                { location.pathname == "/" ?
                    <Link m='2em' fontSize='2xl' href="/#message" color='white'>The Message</Link>
                :
                    <Link as={ReactLink} m='2em' fontSize='2xl' to="/" color='white'>Home</Link>
                }

                {isAuthenticated ?
                    <HStack m='2em'>
                        <Avatar src={user.picture}  onClick={onOpen} cursor='pointer'/>
                        <Text fontSize='2xl' color='white'  onClick={onOpen} cursor='pointer'>{user.name}</Text>
                    </HStack>
                    :
                    <HStack m='2em'>
                        <Link m='2em' fontSize='2xl' href="#" color='white' onClick={() =>  loginWithRedirect({appState: { targetUrl: location.pathname }})} >Log In</Link>
                    </HStack>
                }

            </Flex>
            {user &&  <Modal onClose={onClose} isOpen={isOpen} isCentered size='xs'>
                <ModalOverlay />
                <ModalContent backgroundColor='blackAlpha.800' borderRadius='1em'>
                    {/*<ModalHeader>Modal Title</ModalHeader>*/}
                    <ModalCloseButton colorScheme='purple' >
                        <Button colorScheme='purple' variant='ghost' onClick={onClose}><CloseIcon/></Button>

                    </ModalCloseButton>
                    <ModalBody m='2em'>
                        <VStack alignItems='start' gap='1em'>
                            <Avatar src={user.picture} />
                            <Text color='white'>{user.name}</Text>
                            <Text color='white'>{user.email}</Text>
                        </VStack>
                        <Flex mt='3em' justifyContent='center'>
                            <Button colorScheme='purple' onClick={ () => logout({returnTo: window.location.origin})}>Logout</Button>
                        </Flex>
                    </ModalBody>
                </ModalContent>
            </Modal>}

        </>
    );
};

export default TopBar;
