import React from 'react';
import {Box, Flex, Table, TableContainer, Tbody, Td, Text, Th, Thead, Tr,} from "@chakra-ui/react";
import PropTypes from "prop-types";

const SnowTable = ({firstYear, firstData, secondYear, secondData}) => {

    const snowCm = (data, monthNum) => {
        return Object.values(data[monthNum]).slice(0, -4).reduce((a, b) => a + b, 0);
    }
    return (
        <Flex justifyContent={'space-evenly'}>
            <Box>
                <Text textAlign='center'>{firstYear}</Text>
                <TableContainer>
                    <Table>
                        <Thead>
                            <Tr>
                                <Th>měsíc</Th>
                                <Th>sníh (cm)</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            <Tr>
                                <Td>listopad</Td>
                                <Td>{snowCm(firstData,0)}</Td>
                            </Tr>
                            <Tr>
                                <Td>prosinec</Td>
                                <Td>{snowCm(firstData,1)}</Td>
                            </Tr>
                            <Tr>
                                <Td>leden</Td>
                                <Td>{snowCm(firstData,2)}</Td>
                            </Tr>
                            <Tr>
                                <Td>unor</Td>
                                <Td>{snowCm(firstData,3)}</Td>
                            </Tr>
                            <Tr>
                                <Td>brezen</Td>
                                <Td>{snowCm(firstData,4)}</Td>
                            </Tr>
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
            <Box>
                <Text textAlign='center'>{secondYear}</Text>
                <TableContainer>
                    <Table>
                        <Thead>
                            <Tr>
                                <Th>měsíc</Th>
                                <Th>sníh (cm)</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            <Tr>
                                <Td>listopad</Td>
                                <Td>{snowCm(secondData,0)}</Td>
                            </Tr>
                            <Tr>
                                <Td>prosinec</Td>
                                <Td>{snowCm(secondData,1)}</Td>
                            </Tr>
                            <Tr>
                                <Td>leden</Td>
                                <Td>{snowCm(secondData,2)}</Td>
                            </Tr>
                            <Tr>
                                <Td>unor</Td>
                                <Td>{snowCm(secondData,3)}</Td>
                            </Tr>
                            <Tr>
                                <Td>brezen</Td>
                                <Td>{snowCm(secondData,4)}</Td>
                            </Tr>
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
        </Flex>
    );
};

export default SnowTable;

SnowTable.propTypes = {
    firstYear: PropTypes.number,
    firstData: PropTypes.arrayOf(
        PropTypes.object
    ),
    secondYear: PropTypes.number,
    secondData: PropTypes.arrayOf(
        PropTypes.object
    ),
};
