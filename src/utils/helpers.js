import { useMemo, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { useAuth0 } from '../react-auth0-spa';

export const useType = () => {
  const { search } = useLocation();
  const type = useMemo(() => {
    return new URLSearchParams(search).get('type');
  }, [search]);
  return type;
};

export const useRest = (type) => {
  const { apiSun, apiTemp, apiAir } = useAuth0();
  const [{ resource, rest }, setRestApi] = useState({});

  useEffect(() => {
    if (!type) {
      setRestApi({
        resource: apiSun,
        rest: 'sunshine',
      });
    } else if (type === 'air') {
      setRestApi({
        resource: apiAir,
        rest: 'wind-speed',
      });
    } else if (type === 'temp') {
      setRestApi({
        resource: apiTemp,
        rest: 'temp-avg',
      });
    }
  }, [type]);

  return [{ resource, rest }, setRestApi];
};
