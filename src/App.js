import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

//import PrivateRoute from './components/PrivateRoute';
import LandingPage from './views/LandingPage';
import { useAuth0 } from './react-auth0-spa';
//import CssBaseline from '@material-ui/core/CssBaseline'; uncomment if you want to use material UI
//import HomePage from './views/HomePage';
import Loading from './components/Loading';
import TopBar from "./components/TopBar";
import {Flex} from "@chakra-ui/react";
import PrivateRoute from "./components/PrivateRoute";
//import HomePage from "./views/HomePage";
import Dashboard from "./views/Dashboard";
import czechImg from "./czech.png";

// const drawerWidth = 240;

const App = () => {
  const { loading } = useAuth0();

  if (loading) {
    return <Loading />;
  }

  return (
      <BrowserRouter>
        {/* <CssBaseline />  */} {/* uncomment if you want to use material UI */}

          <Flex flexDirection='column' background={`url(${czechImg}) no-repeat 0 50%, linear-gradient(180deg, #8BC5FA 27.08%, #EEEEF0 87.5%);`}>
              <TopBar />
              <Flex as='main' flexGrow='1'>
                  <Routes>
                      <Route index path="/" element={<LandingPage />} />
                      <Route path="/dashboard" element={<PrivateRoute component={Dashboard} />} />
                  </Routes>
              </Flex>
          </Flex>
      </BrowserRouter>
  )
};

export default App;
