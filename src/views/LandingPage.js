import React from 'react';
//import { useAuth0 } from '../react-auth0-spa';
//import { useNavigate } from 'react-router-dom';
import {Box, Button, Flex, Heading, Image, Text, VStack} from '@chakra-ui/react';
import {Link} from 'react-router-dom';
import snowpersonImg from '../snowperson.png';
import {headerHeight} from "../components/TopBar";

const LandingPage = () => {
  //const navigate = useNavigate();
  //const { user } = useAuth0();
  // useEffect(() => {
  //   if (user) {
  //     navigate('/home-page');
  //   }
  // }, [user]);

  const msgStyle = {
      fontSize: '3xl',
      textAlign: 'center',
      fontWeight: 'bold',
      color: 'white'
  };

  return (
      <Flex flexDirection='column' mt={`-${headerHeight}`}>
        <Flex flexDirection='row' background='linear-gradient(180deg, #8BC5FA 27.08%, #EEEEF0 87.5%);' h='100vh'>
            <Flex flexBasis='40%' alignItems='center'>
                <Image m='10em' maxH='100%' src={snowpersonImg}/>
            </Flex>
            <Flex flexDirection='column' maxW='60em' justifyContent='center'  flex='1 1 auto'>
                <Heading as='h1' size='4xl' textAlign='center' color='white'>Show them that the Climate change isn’t a joke!</Heading>
                <Heading as='h2' size='3xl' textAlign='center' my='1em' color='white'>fun & interactive way</Heading>
                <Box textAlign='center' mt='2em'>
                    <Button as={Link} to={"/dashboard"} colorScheme='red' size='lg'>
                        Let&apos;s start
                    </Button>
                </Box>
            </Flex>
        </Flex>
        <VStack id='message' h='100vh' justifyContent='center' background='linear-gradient(0deg, #8BC5FA 27.08%, #EEEEF0 87.5%);' gap='2em'>
            <Text {...msgStyle}>Climate change is not a joke!</Text>

            <Text {...msgStyle}>
                Even though we live in the 21st century, many people still don’t believe
                the obvious statistics about global warming.
            </Text>

            <Text {...msgStyle}>
                A tool to show these statistics interactively might be actually handy.
            </Text>

            <Text {...msgStyle}>
                Our developer and design team is providing this solution through a Snowmen counter!
            </Text>

            <Text {...msgStyle}>
                How? It’s easy! Select a day in the past and see the
                number of snowmen which could be built that day!
            </Text>

            <Text {...msgStyle}>
                It doesn’t matter if your audience is your grumpy grandpa or a boss of
                a large textile company. Everyone will get the message!
            </Text>

            <Text {...msgStyle}>Send the message!</Text>
        </VStack>
      </Flex>
  );
};

export default LandingPage;
