import React, {useState} from 'react';
import data from '../data/snow.json';
import {
    Box,
    Flex,
    RangeSlider,
   // RangeSliderFilledTrack,
    RangeSliderThumb,
    RangeSliderTrack,
    HStack,
    Img,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalCloseButton,
    ModalBody,
} from '@chakra-ui/react';
import YearPreview from "../components/YearPreview";
import chartImg from "../chart.png";
import tableImg from "../table.png";
import Chart from "../components/Chart";
import SnowTable from "../components/SnowTable";

const Dashboard = () => {
    //const navigate = useNavigate();
    //const { user } = useAuth0();
    // useEffect(() => {
    //   if (user) {
    //     navigate('/home-page');
    //   }
    // }, [user]);
    const getYear = (year) => {
        return data.filter(row => (row.year == year - 1 && [11, 12].includes(row.month)) || row.year == year && [1,2,3].includes(row.month))
            .sort((a,b) => a.month - b.month)
            .sort((a,b) => a.year - b.year);
    }

    const [firstYear, setFirstYear] = useState(1962);
    const [secondYear, setSecondYear] = useState(2018);
    const firstData = getYear(firstYear)
    const secondData = getYear(secondYear);


    const { isOpen: isChartOpen, onOpen: onChartOpen, onClose: onChartClose } = useDisclosure();
    const { isOpen: isTableOpen, onOpen: onTableOpen, onClose: onTableClose } = useDisclosure();

    return (
        <>
            <Flex flexDirection='column' h='100vh' margin={'3em auto'} minW={'60em'}>
                <Flex fontSize={'1.4em'} w={'30em'} alignSelf={'center'}>
                    <Box color='white'>1962</Box>
                    <RangeSlider aria-label={['min', 'max']} min={1962} max={2018} step={1} defaultValue={[1962, 2018]}
                                 onChange={([first, second]) => {
                                     setFirstYear(first);
                                     setSecondYear(second);
                                 }}
                                 mx={'2em'}>
                        <RangeSliderTrack height='10px' borderRadius='10px' background='#6A97C0'>
                            {/*<RangeSliderFilledTrack />*/}
                        </RangeSliderTrack>
                        <RangeSliderThumb index={0} w='3.5em' h='1.8em' borderRadius='0.3em' backgroundColor='#0D0D0D' color='white'>{firstYear}</RangeSliderThumb>
                        <RangeSliderThumb index={1} w='3.5em' h='1.8em' borderRadius='0.3em' backgroundColor='#0D0D0D' color='white'>{secondYear}</RangeSliderThumb>
                    </RangeSlider>
                    <Box color='white'>2018</Box>
                </Flex>
                <HStack gap={'2em'} justifyContent={'space-between'} my={'3em'}>
                    <YearPreview year={firstYear} data={firstData}/>
                    <YearPreview year={secondYear} data={secondData}/>
                </HStack>
                <Flex justifyContent={'space-around'}>
                    <Box>
                        <Img src={chartImg} w={'6em'} onClick={onChartOpen} cursor='pointer'/>
                    </Box>
                    <Box>
                        <Img src={tableImg} w={'6em'} onClick={onTableOpen} cursor='pointer'/>
                    </Box>
                </Flex>
            </Flex>
            <Modal onClose={onChartClose} isOpen={isChartOpen} isCentered>
                <ModalOverlay />
                <ModalContent maxW="65em" py={'2em'}>
                    <ModalCloseButton />
                    <ModalBody>
                       <Chart firstYear={firstYear} firstData={firstData} secondYear={secondYear} secondData={secondData} />
                    </ModalBody>
                </ModalContent>
            </Modal>

            <Modal onClose={onTableClose} isOpen={isTableOpen} isCentered size={'xl'}>
                <ModalOverlay />
                <ModalContent py={'2em'}>
                    <ModalCloseButton />
                    <ModalBody>
                        <SnowTable firstYear={firstYear} firstData={firstData} secondYear={secondYear} secondData={secondData} />
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>

    );
};

export default Dashboard;
