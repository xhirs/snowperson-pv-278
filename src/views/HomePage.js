import React from 'react';
import { useType, useRest } from '../utils/helpers';
import {Box, Flex, Heading} from "@chakra-ui/react";

const HomePage = () => {
  const type = useType();
  const [{ resource, rest }] = useRest(type);
  console.log('==========');
  console.log('type ↓');
  console.log(type);
  console.log('==========');
  console.log('resource ↓');
  console.log(resource);
  console.log('==========');
  console.log('rest ↓');
  console.log(rest);
  console.log('==========');

  return (
    <Flex alignItems='center' justifyContent='center' w='full'>
      <Box>
        <Heading color='red'>Dashboard</Heading>
      </Box>
    </Flex>
  );
};

export default HomePage;
